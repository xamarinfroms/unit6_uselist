﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unit5.Core;

namespace Unit5_DataBinding
{
    public class ProductsViewModel : INotifyPropertyChanged
    {
        public ProductsViewModel()
        {
            this.Items = ProductFactory.CreateProducts();
            this.SelectedItem = this.Items.Skip(1).FirstOrDefault();
        }
        public IList<Product> Items { get; set; }

        public Product SelectedItem { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
